#include <cstdint>
#include <vector>
#include <queue> 
#include <algorithm>
#include <memory>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstring>

using namespace std;


// Aux function to keep track of mixture indices while sorting
// <https://stackoverflow.com/questions/1577475/c-sorting-and-keeping-track-of-indexes>
vector<size_t> sort_indices(const vector<uint32_t> &v){
  // initialize original index locations
  vector<size_t> idxVec(v.size());
  iota(idxVec.begin(), idxVec.end(), 0);

  // sort indexes based on comparing values in v
  sort(idxVec.begin(), idxVec.end(),
       [&v](size_t i1, size_t i2) {return v[i1] > v[i2];});

  return idxVec;	
}

void countOccurencesAndSort(ifstream& inFile, string inFileName, 
							size_t numMixIndices, ofstream& outFile, string outFileName){
	inFile.open(inFileName.c_str());
	outFile.open(outFileName.c_str());
	// 1.) Count occurences
	vector<uint32_t> counts(numMixIndices,0);
	string currLine;
	while(getline(inFile, currLine)){
		while( !currLine.empty()){
			size_t idx;
			size_t currNumber =
						stoi(currLine, &idx, 10); // 10 = base.
			counts[currNumber]++;
			currLine = currLine.substr(idx);
		}
	}

	// 2.) Sort by number of occurences -> file
	vector<size_t> sortedIndices = sort_indices(counts);
	sort(counts.begin(), counts.end(), greater<uint32_t>());
	if (outFile.is_open()){
		for(unsigned int i = 0; i < counts.size(); i++){
			outFile << to_string(sortedIndices[i]) + ";" + to_string(counts[i]) << endl; 
		}
	}
	else
		cout << "File could not be opened" << endl;

	inFile.close();
	outFile.close();
}

int main(/*int argc, char const *argv[]*/)
{
	bool align_WW_to_Phon = true;

	// 1.) Streams for files
	ifstream inFile_WW ;
	ofstream outFile_WW;
	ofstream alignFile_Phon("alignment.phonem");
	ofstream outFile_Phon;

	// c)
	u_int16_t numWWs = 100;
	vector<pair<uint16_t,uint16_t>> map_WW_to_Phon(numWWs);
	uint16_t mapIdx = 0;
	map_WW_to_Phon[mapIdx]= make_pair(0,0); // mixture index for silence remains same 
	mapIdx++;
	if(align_WW_to_Phon){
		// Determine mapping WW indices <--> Phoneme indices
		ifstream models_WW("whole-word.models");
		ifstream lexicon("digits.lexicon");

		string currLine_WW;
		string currLine_LEX;
		while(getline(models_WW, currLine_WW)){
			// a) Construct array of mixture indices for digit in WW model 
			queue<uint16_t> curr_mixIndices_WW;
			bool getDigit = true;
			string currDigit;
			string restOfLine_WW = currLine_WW;
			size_t nextPos = 0; // necessary for parsing correct substring as integers
			while(!restOfLine_WW.empty()){
				if(getDigit){
					istringstream iss(restOfLine_WW);
					iss >> currDigit;
					nextPos += currDigit.length();
					getDigit = false;
				}
				else{
					uint16_t curr_mixIdx =
						stoi(restOfLine_WW, &nextPos, 10); 
					curr_mixIndices_WW.push(curr_mixIdx);
				}
				restOfLine_WW = restOfLine_WW.substr(nextPos); // traversal
			} // while(!restOfLine_WW.empty())

			// b.) Look up transcriptions of each digit in lexicon
			getline(lexicon, currLine_LEX);
			string currPhon;
			string restOfLine_LEX = currLine_LEX;
			stringstream iss(restOfLine_LEX);
			string temp;
			iss >> temp;
			// skip first substring since it's no phonem but a digit
			size_t nextPos_LEX = temp.length();
			restOfLine_LEX = restOfLine_LEX.substr(nextPos_LEX);
			while(iss >> currPhon){
				cout << currPhon +": " << endl;
				// c.) Find mixture indices for each phonem 
				bool phonemeFound = false;
				ifstream models_Phon("phoneme.models");
				string currLine_Phon;
				while(!phonemeFound && getline(models_Phon, currLine_Phon)){
					stringstream iss2(currLine_Phon);
					string currPhonInModels;
					iss2 >> currPhonInModels;
					cout << "currLine_Phon: " + currLine_Phon << endl;
					if(currPhonInModels == currPhon){
						cout << "phonemeFound!" << endl;
						// d.) Map associated mixture indices of phoneme to WW mixtures
						size_t nextPos_Phon = currPhon.length();
						string restOfLine_Phon = currLine_Phon.substr(nextPos_Phon);
						while(!restOfLine_Phon.empty()){
							uint16_t curr_mixIdx_Phon =
								stoi(restOfLine_Phon, &nextPos_Phon, 10); 
							cout << "Values in pair are: (" + to_string(curr_mixIndices_WW.front()) + "," + 
																		 to_string(curr_mixIdx_Phon) + ")" << endl;
																		 
							map_WW_to_Phon[mapIdx] = make_pair(curr_mixIndices_WW.front(),curr_mixIdx_Phon);
							mapIdx++;							
							curr_mixIndices_WW.pop();

							restOfLine_Phon = restOfLine_Phon.substr(nextPos_Phon);
						}
						phonemeFound = true;
					}

				}

			}
		} // while(getline(models_WW, currLine_WW))
	}


	countOccurencesAndSort(inFile_WW, "alignment.word", numWWs, outFile_WW, "indexCounts_WW.csv"); // b)

	// c)
	// Translate alignment.word --> alignment.phonem
	inFile_WW.open("alignment.word");
	string currLine_WW_Align;
	while(getline(inFile_WW, currLine_WW_Align)){
		while( !currLine_WW_Align.empty()){
			size_t idx;
			size_t currNumber =
						stoi(currLine_WW_Align, &idx, 10); 
			currLine_WW_Align = currLine_WW_Align.substr(idx);
			alignFile_Phon << to_string(map_WW_to_Phon[currNumber].second); 
			if(!currLine_WW_Align.empty())
				alignFile_Phon << " "; // space if not last number
		}
		alignFile_Phon << "\n";
	}
	inFile_WW.close();

	size_t numPhonemes = 58;
	ifstream alignFile_Phon_IN;
	countOccurencesAndSort(alignFile_Phon_IN, "alignment.phonem", numPhonemes, outFile_Phon, "indexCounts_Phon.csv");

	return 0;
}
