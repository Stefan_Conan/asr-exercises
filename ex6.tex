\documentclass[a4paper]{article}

\usepackage[T1]{fontenc}
\usepackage{amsfonts}
\usepackage{comment}
%\usepackage{times}
%\usepackage{changepage}
\usepackage[english]{babel}
%\usepackage[a4paper, top=2.5cm, bottom=2.5cm, left=2.2cm, right=2.2cm]%
\usepackage[margin=1in]{geometry}
\usepackage[table]{xcolor}
\usepackage{mathtools, fancyhdr, lastpage, amssymb, float}
\usepackage{tikz}
\usepackage[amsmath, amsthm, thmmarks]{ntheorem}
\usepackage{etoolbox}
%\usepackage{parskip}
\usepackage{graphicx}
\usepackage{enumitem}
\graphicspath{ {images/} }


\usepackage[utf8]{inputenc}


% footer stuff
\pagestyle{fancy}
\fancyhf{}
\cfoot{Page \thepage \hspace{1pt} of \pageref{LastPage}}
\renewcommand{\headrulewidth}{0pt}

\newcommand{\horrule}[1]{\rule{\linewidth}{#1}}

\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\E}{\operatorname{E}}
\newcommand{\Var}{\operatorname{Var}}
\newcommand{\Cov}{\operatorname{Cov}}
\newcommand{\SD}{\operatorname{SD}}
\newcommand{\spp}{\operatorname{support}}
\definecolor{Gray}{gray}{0.85}

\setlength\parindent{0pt}

\begin{document}

\title{
		%\vspace{-1in}
		\usefont{OT1}{bch}{b}{n}
		\normalfont \normalsize \textsc{Automatic Speech Recognition, WS 2017/18, RWTH Aachen} \\
		\horrule{2pt} \\[0.4cm]
		\huge \textbf{Exercise 6} \\
		\horrule{2pt} \\[0.5cm]
}

\author{
\textbf{Stefan Kohnen} \\ 304263 \\ stefan.kohnen@rwth-aachen.de
\and
\textbf{Tina Raissi} \\ 365814\\ tina.raissi@rwth-aachen.de
\and
\textbf{Deyang Sheng} \\ 333537  \\deyang.sheng@rwth-aachen.de \\    \
}

\maketitle

\section*{Task 1}
\begin{enumerate}[label=\alph*)]
	\item % a)
	\textit{Claim}: 
	\begin{equation*}
		\mathcal{LL}(x_1^T; \mu_m, \sigma_m^2, m = 1,2,3) = \sum_{m=1}^3 \sum_{t=s_m}^{e_m} log \mathcal{N}(x_t; \mu_m, \sigma_m^2).
	\end{equation*}
	\begin{proof}
		First, consider the log-likelihood for each segment $m \in \{1,2,3 \} $ separately:
		\begin{equation*}
			\mathcal{LL}(x_{s_m}^{e_m}; \mu_m, \sigma_m^2) = \sum_{t=s_m}^{e_m} log \mathcal{N}(x_t; \mu_m, \sigma_m^2).
		\end{equation*} This is given by the definition of log-likelihood of a sequence of observations.
		The starting and ending time indices $s_m$ resp. $e_m$ are constant. It is given beforehand that for each segment 
		we assume a single Gaussian distribution. Considering three different segments having independent distribution parameters
		is analogous to assume statistical independence of several components in the whole sequence of feature vectors (see slide p. 146).
		We have to keep in mind that on slide p. 146 we "connect" several distributions by taking the \textbf{product} of their 
		respective likelihoods. That's valid because of their independency. However, since we are interested in the \textbf{log}-likelihood, 
		we take the logarithm, transforming the product into a sum. Thus, finally we come to the final formula of the overall likelihood:
		\begin{equation*}
		\mathcal{LL}(x_1^T; \mu_m, \sigma_m^2, m = 1,2,3) = \sum_{m=1}^3 \sum_{t=s_m}^{e_m} log \mathcal{N}(x_t; \mu_m, \sigma_m^2).		
		\end{equation*}
	\end{proof}
	
	\item % b)
	From a) we know the following relationship: 
	\begin{equation*}
		\mathcal{LL}(x_1^T; \mu_m, \sigma_m^2, m = 1,2,3) = \sum_{m=1}^3 \sum_{t=s_m}^{e_m} log \mathcal{N}(x_t; \mu_m, \sigma_m^2).
	\end{equation*}
	Thus, maximizing the left hand side is equivalent to maximizing each addend separately for $m \in \{1,2,3\}$, i. e.:
	\begin{equation*}
			\underset{\mu_m, \sigma_m^2}{\text{max}} \sum_{t=s_m}^{e_m} log \mathcal{N}(x_t; \mu_m, \sigma_m^2)
			\text{, for each } m \in \{1,2,3\}.
	\end{equation*} 
	Keep in mind that the maximization is independent of the starting and ending time indices $s_m$ resp. $e_m$. 
	This is due to the fact that these are considered constant. \\
	\textbf{Model B}: \\
	In the following, we will derive the estimators $\hat{\mu}_m, \hat{\sigma}_m^2$ such that 
	$\mathcal{LL}(x_{s_m}^{e_m}; \hat{\mu}_m, \hat{\sigma}_m^2)$ is maximal. 
	To this end, we transform the maximization problem into a minimization problem, i. e. consider the negative log-likelihood. 
	This allows us to use an analogous formula to the one on slide 147:
	\begin{equation*}
		-\mathcal{LL}(x_{s_m}^{e_m}; \mu_m, \sigma_m^2) 
		= \frac{1}{2} \sum_{t=s_m}^{e_m} \left(\frac{x_t-\mu_m}{\sigma_m} \right)^2 + 
		\frac{1}{2} \sum_{t=s_m}^{e_m} log \left( 2 \pi \sigma_m^2 \right).
	\end{equation*}
	This formula now undergoes some transformations for easier use:
	\begin{multline*}
	\frac{1}{2} \sum_{t=s_m}^{e_m} \left(\frac{x_t-\mu_m}{\sigma_m} \right)^2 + 
	\frac{1}{2} \sum_{t=s_m}^{e_m} log \left( 2 \pi \sigma_m^2 \right) \\
	= \frac{1}{2 \sigma_m^2}	\sum_{t=s_m}^{e_m} \left( x_t-\mu_m \right)^2 +
	\frac{e_m - s_m + 1}{2} log \left( 2 \pi \right) + \frac{e_m - s_m + 1}{2} log \left( \sigma_m^2 \right).
	\end{multline*}
	In the following, we will compute the partial derivatives of the above formula 
	with respect to $\mu_m$ and $\sigma_m^2$ and set them to zero. 
	This results in computing the estimators $\hat{\mu}_m$ and $\hat{\sigma}_m^2$.
	\begin{multline*}
	-\frac{\partial}{\partial \mu_m} \mathcal{LL}(x_{s_m}^{e_m}; \mu_m, \sigma_m^2) 
	= \frac{\partial}{\partial \mu_m} \left( \frac{1}{2 \sigma_m^2}	
	\sum_{t=s_m}^{e_m} \left( x_t-\mu_m \right)^2 + \frac{e_m - s_m + 1}{2} log \left( 2 \pi \right) + 
	\frac{e_m - s_m + 1}{2} log \left( \sigma_m^2 \right) \right) \\
	= -\frac{1}{\sigma_m^2} \sum_{t=s_m}^{e_m}\left( x_t-\mu_m \right) 
	= -\frac{1}{\sigma_m^2} \left( \sum_{t=s_m}^{e_m} x_t - (e_m - s_m + 1)\mu_m \right) \overset{!}{=}  0 
	\Leftrightarrow \sum_{t=s_m}^{e_m} x_t - (e_m - s_m + 1)\mu_m = 0 \\ 
	\Leftrightarrow \mu_m = \frac{1}{e_m - s_m + 1} \sum_{t=s_m}^{e_m} x_t = \hat{\mu}_m.
	\end{multline*}
	\begin{multline*}
		-\frac{\partial}{\partial \sigma_m^2} \mathcal{LL}(x_{s_m}^{e_m}; \mu_m, \sigma_m^2) 
		= \frac{\partial}{\partial \sigma_m^2} \left( \frac{1}{2 \sigma_m^2} 
		\sum_{t=s_m}^{e_m} \left( x_t-\mu_m \right)^2 + \frac{e_m - s_m + 1}{2} log \left( 2 \pi \right) + 
		\frac{e_m - s_m + 1}{2} log \left( \sigma_m^2 \right) \right)  \\		
		= \frac{e_m - s_m + 1}{2 \sigma_m^2} + \left[ \frac{1}{2} 
		\sum_{t=s_m}^{e_m} \left( x_t- \mu_m \right)^2  \right] 
		\frac{\partial}{\partial \sigma_m^2} \left( \frac{1}{\sigma_m^2} \right) 
		= \frac{e_m - s_m + 1}{2 \sigma_m^2} + \left[ \frac{1}{2} 
		\sum_{t=s_m}^{e_m} \left( x_t- \mu_m \right)^2  \right] 
		\left( - \frac{1}{(\sigma_m^2)^2} \right) \\
		= \frac{e_m - s_m + 1}{2 \sigma_m^2} - \left[ \frac{1}{2} 
		\sum_{t=s_m}^{e_m} \left( x_t- \mu_m \right)^2  \right] 
		 \left(  \frac{1}{(\sigma_m^2)^2} \right) 
		= \frac{1}{2 \sigma_m^2} \left[ (e_m - s_m + 1) - \frac{1}{\sigma_m^2} 
		\sum_{t=s_m}^{e_m} \left( x_t- \mu_m \right)^2  \right] \overset{!}{=}  0 \\
		\overset{\sigma_m \neq 0}{\Leftrightarrow} 
		(e_m - s_m + 1) - \frac{1}{\sigma_m^2} \sum_{t=s_m}^{e_m} \left( x_t- \mu_m \right)^2 = 0 
		\Leftrightarrow \sigma_m^2 =
		\frac{1}{e_m - s_m + 1}\sum_{t=s_m}^{e_m} \left( x_t- \mu_m \right)^2 = \hat{\sigma}_m^2.
	\end{multline*}
	\textbf{Model A}: \\
	Estimating $\hat{\mu}_m$ for model A is equivalent to estimating it for model B (see above). As we have a pooled variance $\sigma^2$ now, we cannot any longer maximize the log-likelihood by just maximizing each addend with respect to $\sigma_m^2$ in the formula obtained from a). Thus, we derive the whole formula with respect to $\sigma^2$:
	\begin{multline*}
		-\frac{\partial}{\partial \sigma^2} \mathcal{LL}(x_1^T; \mu_m, \sigma^2, m = 1,2,3) 
		= -\frac{\partial}{\partial \sigma^2} 
		\sum_{m=1}^3 \sum_{t=s_m}^{e_m} log \mathcal{N}(x_t; \mu_m, \sigma_m^2)\\
		\overset{(*)}{=} \sum_{m=1}^3 
		\frac{1}{2 \sigma^2} \left[ (e_m - s_m + 1) - \frac{1}{\sigma^2} 
		\sum_{t=s_m}^{e_m} \left( x_t- \mu_m \right)^2  \right] \overset{!}{=}  0
		\overset{\sigma \neq 0}{\Leftrightarrow} 
		\sum_{m=1}^3 \left[(e_m - s_m + 1) - \frac{1}{\sigma^2} 
		\sum_{t=s_m}^{e_m} \left( x_t- \mu_m \right)^2 \right] = 0 \\
		\Leftrightarrow \sigma^2 \sum_{m=1}^3 (e_m - s_m + 1) 
		= \sum_{m=1}^3 \sum_{t=s_m}^{e_m} (x_t - \mu_m)^2 
		\Leftrightarrow \sigma^2 = \sum_{m=1}^3 
		\frac{\sum_{t=s_m}^{e_m}(x_t - \mu_m)^2}{\sum_{m=1}^3 (e_m - s_m + 1)}\\ 
		\left(=  \frac{\sum_{m=1}^3 (e_m - s_m + 1) \sigma_m^2}
		{T}      \right) = \hat{\sigma}^2,
	\end{multline*}
	where $T$ is the length of the original input sequence. Notes to $(*)$: 
	the necessary steps to transform this formula were performed before (see model B), so for simplicity we leave them out.

	
	\item % c)
	We try to find the optimal boundaries by maximizing the likelihood and therefore the log-likelihood
	\begin{equation*}
	\mathcal{LL}(x_1^T; \mu_m, \sigma^2, m = 1,2,3) = \sum_{m=1}^3 \sum_{t=s_m}^{e_m} \log \mathcal{N}(x_t; \mu_m, \sigma_m^2).
	\end{equation*}
	It holds
	\begin{equation*}
	\mathcal{N}(x; \mu, \sigma) = \frac{1}{\sqrt{(2 \pi \sigma^2)}} e^{-\frac{1}{2}(\frac{x-\mu}{\sigma})^2}.
	\end{equation*}
	Therefore
	\begin{align*}
	& \max_{e_m,s_m} \mathcal{LL}(x_1^T; \mu_m, \sigma^2, m = 1,2,3) \\
	& = \max_{e_m,s_m} \sum_{m=1}^3 \sum_{t=s_m}^{e_m} \log \mathcal{N}(x_t; \mu_m, \sigma^2)\\
	& = \max_{e_m,s_m} \sum_{m=1}^3 \sum_{t=s_m}^{e_m} \log \left (  \frac{1}{\sqrt{(2 \pi \sigma^2)}} e^{-\frac{1}{2}(\frac{x_t-\mu_m}{\sigma})^2} \right )\\
	& = \max_{e_m,s_m} \sum_{m=1}^3 \sum_{t=s_m}^{e_m} \mathrm{const} + \left (  -\frac{1}{2}(\frac{x_t-\mu_m}{\sigma})^2 \right )\\
	& = \max_{e_m,s_m} \sum_{m=1}^3 \sum_{t=s_m}^{e_m} \left (  -(\frac{x_t-\mu_m}{\sigma})^2 \right )\\
	& = \min_{e_m,s_m} \sum_{m=1}^3 \sum_{t=s_m}^{e_m} \left (  (\frac{x_t-\mu_m}{\sigma})^2 \right ).\\
	\end{align*}
	Strictly speaking, the optimal $\sigma$ (in regards to arbitrary fixed $e_m$ and $s_m$) also depends on $e_m$ and $s_m$. However, we can assume that there exists an optimal $\sigma^*$ (in regards to the optimal $e_m$ and $s_m$), which we already found in b).
	\begin{align*}
	& \max_{e_m,s_m} \mathcal{LL}(x_1^T; \mu_m, \sigma^2, m = 1,2,3) \\
	& = \min_{e_m,s_m} \sum_{m=1}^3 \sum_{t=s_m}^{e_m} \left (  (\frac{x_t-\mu_m}{\sigma})^2 \right )\\
	& = \min_{e_m,s_m} \sum_{m=1}^3 \sum_{t=s_m}^{e_m} \left (  (\frac{x_t-\mu_m}{\sigma^*})^2 \right )\\
	& = \min_{e_m,s_m} \sum_{m=1}^3 \sum_{t=s_m}^{e_m} \left ( x_t-\mu_m \right )^2\\
	& = \min_{e_m,s_m} \sum_{m=1}^3 \sum_{t=s_m}^{e_m} h(x_t;\mu_{s_m,e_m})\\
	& = \text{ squared error criterion from exercise 5}
	\end{align*}
	\item We first write the cost under model B by using its definition and for every $m \in \{1,2,3\}$.
		\begin{align*}
		& - \mathcal{LL}(x_{s_m}^{e_m}; \hat{\mu}_, \hat{\sigma}_m^2)\\
		&= - \sum_{t=s_m}^{e_m} \log \mathcal{N}(x_t; \hat{\mu}_m, \hat{\sigma}_m^2)\\
		& = \frac{1}{2} \sum_{t=s_m}^{e_m} \left(\frac{x_t-\hat{\mu}_m}{\hat{\sigma}_m} \right)^2 + 
		\frac{1}{2} \sum_{t=s_m}^{e_m} log \left( 2 \pi \hat{\sigma}_m^2 \right)\\
		& = \frac{1}{2} \big[\frac{1}{\hat{\sigma}_m^2}  \sum_{t=s_m}^{e_m} \left(x_t-\hat{\mu}_m\right)^2+ (e_m-s_m+1)\log 2\pi + (e_m-s_m+1)\log \hat{\sigma}_m^2 \big]\\
		& = \frac{e_m-s_m+1}{2} \big[\frac{1}{(e_m-s_m+1)\hat{\sigma}_m^2}  \sum_{t=s_m}^{e_m} \left(x_t-\hat{\mu}_m\right)^2+ \log 2\pi + \log \hat{\sigma}_m^2 \big].
		\end{align*}
		We know due to our calculation in (b) that $\hat{\sigma}_m^2 = 	\frac{1}{e_m - s_m + 1}\sum_{t=s_m}^{e_m} \left( x_t- \hat{\mu}_m \right)^2$, so we substitute it and we have
		\begin{align*}
		= \frac{e_m-s_m+1}{2} \big[ 1 + \log 2\pi + \log \big(\frac{1}{e_m - s_m + 1}\sum_{t=s_m}^{e_m} \left( x_t- \hat{\mu}_m \right)^2\big)\big].
		\end{align*}
	
\end{enumerate}

\end{document}
