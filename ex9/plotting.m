clear all; clc;
close all;

figure('Position',[200,100,1200,600])
fileID = fopen('indexCounts_Tri.csv');
strdata = textscan(fileID, '%s %s','delimiter', ';', 'EmptyValue', -Inf);
data = cellfun(@str2num,[strdata{1},strdata{2}]);
bar(data(:,2))
title('Triphones: counts of mixture indices');
ylabel('# of occurences');
ax = gca;
ax.XAxis.Visible = 'off';

figure('Position',[200,100,1200,600])
fileID = fopen('indexCounts_Tied_Tri.csv');
strdata = textscan(fileID, '%s %s','delimiter', ';', 'EmptyValue', -Inf);
data = cellfun(@str2num,[strdata{1},strdata{2}]);
bar(data(:,2))
title('Tied Triphones: counts of mixture indices');
ylabel('# of occurences');
ax = gca;
ax.XAxis.Visible = 'off';