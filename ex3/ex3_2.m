s = [0 1 2 1 4 5 5 4 3 1 2 2 0];
t = [0 0 0 2 2 2 1 3 4 5 4 3 2 1 1 1 2 2 2 1 0];
d = zeros(size(s,2),size(t,2));
for i = 1:size(s,2)
    for j = 1:size(t,2)
        d(i,j) = abs(s(i)-t(j));
    end
end
disp(flipud(d))

D = NaN(size(s,2),size(t,2));
D(1,1) = 0;
for j = 2:size(t,2)
    for i = 1:min(2*j,size(s,2))
        if i > 2
            D(i,j) = d(i,j) + min([D(i,j-1),D(i-1,j-1),D(i-2,j-1)]);
        elseif i > 1
            D(i,j) = d(i,j) + min([D(i,j-1),D(i-1,j-1)]);
        elseif i == 1
            D(i,j) = d(i,j) + D(i,j-1);
        end
    end
end
D_ex = flipud(D);
